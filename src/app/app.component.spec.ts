import { screen, fireEvent, render } from '@testing-library/angular';
import userEvent from '@testing-library/user-event';
import { AppComponent } from './app.component';
import { ButtonComponent } from './button/button.component';
import { TextFieldComponent } from './text-field/text-field.component';

describe('AppComponent', () => {
  it('shows the note that has been created', async () => {
    const message = 'Hola Mundo';
    await renderComponent();

    await writeText(message);
    clickAddButton();

    expect(screen.queryByText(message)).toBeInTheDocument();
  });

  it('does not shows text note while typing', async () => {
    const message = 'Hola Mundo';
    await renderComponent();

    await writeText(message);

    expect(screen.queryByText(message)).not.toBeInTheDocument();
  });

  it('show more than one note', async () => {
    const messageOne = 'Hola Mundo';
    const messageTwo = 'Adios Mundo';
    await renderComponent();

    await addNote(messageOne);
    await userEvent.clear(screen.getByRole('textbox', { hidden: true }));
    await addNote(messageTwo);

    expect(screen.queryByText(messageOne)).toBeInTheDocument();
    expect(screen.queryByText(messageTwo)).toBeInTheDocument();
  });

  const writeText = async (message: string) => {
    await userEvent.type(
      screen.getByRole('textbox', { hidden: true }),
      message
    );
  };

  const addNote = async (message: string) => {
    await writeText(message);
    clickAddButton();
  };

  const clickAddButton = () => {
    fireEvent.click(screen.getByText('Add'));
  };

  const renderComponent = async () => {
    await render(AppComponent, {
      declarations: [TextFieldComponent, ButtonComponent],
    });
  };
});
