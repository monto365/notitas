import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent implements OnInit {
  @Output() onClick: EventEmitter<void> = new EventEmitter<void>();
  @Input() text: string = '';

  constructor() {}

  ngOnInit(): void {}

  btnClick(event?: Event) {
    return this.onClick.emit();
  }
}
