import { render, screen, fireEvent } from '@testing-library/angular';
import { ButtonComponent } from './button.component';

describe('ButtonComponent', () => {
  it('executes the given callback when clicked', async () => {
    const callback = jest.fn();
    const text = 'Hola Soy un boton'
    await render(`<app-button text="${text}" (onClick)="onClick($event)">`, {
      declarations: [ButtonComponent],
      componentProperties: {
        onClick: callback
      },
    });

    fireEvent.click(screen.getByText(text));

    expect(callback).toHaveBeenCalledTimes(1);
  });

  it('Has a custom text', async () => {
    const text = 'Hello'
    
    await render(`<app-button text="${text}">`, {
      declarations: [ButtonComponent]
    });

    expect(screen.getByText(text)).toBeInTheDocument()
  })
});
