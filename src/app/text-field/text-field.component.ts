import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';

@Component({
  selector: 'app-text-field',
  templateUrl: './text-field.component.html',
  styleUrls: ['./text-field.component.scss'],
})
export class TextFieldComponent implements OnInit {
  @Input() content!: string;
  @Output() contentChange = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  sendValueContent(event: Event): void {
    const targetValue = event.target as HTMLTextAreaElement;
    this.contentChange.emit(targetValue?.value);
  }
}
