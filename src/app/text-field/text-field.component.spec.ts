import { render, screen } from '@testing-library/angular';
import { TextFieldComponent } from './text-field.component';
import userEvent from '@testing-library/user-event';

describe('TextFieldComponent', () => {
  it('gets the placeholder by default', async () => {
    const placeholderText = 'Type here';

    await render(TextFieldComponent);

    const placeholderElement = screen.queryByPlaceholderText(placeholderText);
    expect(placeholderElement).toBeInTheDocument();
  });

  it('is writable', async () => {
    const message = 'Hola Mundo';
    await render(TextFieldComponent);

    await userEvent.type(
      screen.getByRole('textbox', { hidden: true }),
      message
    );

    expect(screen.getByRole('textbox', { hidden: true })).toHaveValue(message);
  });

  xit('write a text and clean it', async () => {
    const message = 'Hola Mundo';
    await render(TextFieldComponent);

    await userEvent.type(
      screen.getByRole('textbox', { hidden: true }),
      message
    );

    expect(screen.getByRole('textbox', { hidden: true })).toHaveValue('');
  });
});
