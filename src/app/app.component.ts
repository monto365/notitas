import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'App-notas';
  message: string = '';
  notes: string[] = [];

  buttonClick() {
    this.notes.push(this.message);
  }
}
