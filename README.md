# AppNotas

Commands:

# It builds Docker images from a Dockerfile searching a Dockerfile in the path ".", and tagging the image with the name voicedown

docker build -t voicedown .

# It creates a writeable container layer over the specified image (in this case a image called voicedown), and then starts it using a specified command. 

   # --rm -> Automatically remove the container when it exits
   # -i   -> Create a interactive session
   # -t   -> Emulate a terminal
   # -v ${pwd}:/voicedown -> Mounts the current working directory into the container with the name "voicedown" as a volume
   # -p 4200:4200 -> port Host port : Container Port
   # voicedown -> Image name

docker run --rm -it -v ${pwd}:/voicedown -p 4200:4200 voicedown

