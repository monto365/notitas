.PHONY: build start

build:
	docker build -t voicedown .

start: build
	docker run --rm -it -v $$(pwd)/src:/voicedown/src -p 4200:4200 voicedown

shell: build
	docker run --rm -it -v $$(pwd)/src:/voicedown/src voicedown bash

test: build
	docker run --rm -it -v $$(pwd)/src:/voicedown/src voicedown npm run test

test-watch: build
	docker run --rm -it -v $$(pwd)/src:/voicedown/src voicedown npm run test:watch
